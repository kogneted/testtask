<?php
/**
 * Created by PhpStorm.
 * User: Денис
 * Date: 11.04.2020
 * Time: 18:09
 */

namespace app\models;


use keltstr\simplehtmldom\simple_html_dom_node;

class Transaction
{

    const BUY = 'buy';
    const SELL = 'sell';
    const BALANCE = 'balance';
    const STOP_BUY = 'buy stop';
    const STOP_SELL = 'buy sell';

    private $type; // Тип транзакции

    public $id; // тикет
    public $profit = 0; // профит
    public $commission = 0; // профит
    public $date; // дата
    /**
     * @var simple_html_dom_node
     */
    private $_tr;

    /**
     * Если сделка заверщенная
     * @return bool
     */
    public function isComplete() {return $this->type == self::BUY || $this->type == self::SELL;}

    /**
     * Запись снятия или пополнения баланса
     * @return bool
     */
    public function isTransfer() {return $this->type == self::BALANCE;}


    /**
     * Инициация свойств через конструктор
     * Transaction constructor.
     * @param simple_html_dom_node $tr
     */
    public function __construct(simple_html_dom_node $tr)
    {
        $this->_tr = $tr;
        $this->id = $this->getIntValue(0);
        $this->type = $this->getStringValue(2);

        //$this->commission = $this->getDoubleValue(10);
        // Дата транзацкии в зависимости от типа, либо дата открытия, либо закрытия, если это сделка
        $this->date =  str_replace('.','-',$this->getStringValue($this->isComplete() ? 8 : 1));

        // Профит  в зависимости от типа транзакции
        if($this->isComplete())
            $this->profit = $this->getDoubleValue(13);
        if($this->isTransfer())
            $this->profit = $this->getDoubleValue(4);




    }

    /**
     * @param $index
     * @return int|string
     */
    private function getIntValue($index)
    {
        return ($node = $this->getNode($index))!= null ? (int)$node->text() : "";
    }


    /**
     * Получение строчного значения
     * @param $index
     * @return string
     */
    private function getStringValue($index)
    {
        return ($node = $this->getNode($index))!= null ? $node->text() : "";
    }

    /**
     * Получение дробного значения
     * @param $index
     * @return float
     */
    private function getDoubleValue($index)
    {
        return ($node = $this->getNode($index))!= null ? (double) str_replace(' ','',$node->text()) : null;
    }



    /**
     * Получение объекта узла библиотекипо индексу
     * @param $index
     * @return simple_html_dom_node|null
     */
    private function getNode($index)
    {
        return (count($this->_tr->children()) - 1) >= $index ? $this->_tr->children($index) : null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "$this->date $this->type $this->profit";
    }


    public function validate()
    {
        switch ($this->type){
            case self::SELL:break;
            case self::BUY:break;
            case self::BALANCE:break;
            case self::STOP_SELL:break;
            case self::STOP_BUY:break;
            default:
                return false;
        }

        if($this->profit == null)
            return false;
        if(!is_numeric($this->id))
            return false;


        return true;
    }

}