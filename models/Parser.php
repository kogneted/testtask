<?php
/**
 * Created by PhpStorm.
 * User: Денис
 * Date: 11.04.2020
 * Time: 20:06
 */

namespace app\models;


use keltstr\simplehtmldom\simple_html_dom;
use keltstr\simplehtmldom\simple_html_dom_node;
use keltstr\simplehtmldom\SimpleHTMLDom;
use PHPUnit\Exception;
use yii\base\Model;
use yii\web\UploadedFile;

class Parser extends Model
{
    /**
     * @var UploadedFile
     */
    public $fileHtml;
    private $dom;

    public $transactions = [];
    /**
     * @var simple_html_dom_node[]
     */
    private $nodes;


    public function rules()
    {
        return [
            [['fileHtml'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html','checkExtensionByMimeType' => false],
            [['fileHtml'], 'parseValid'],
        ];
    }

    /**
     * правило проверки документы на нахождени езаписей транзакций
     * @param $attribute
     * @param $params
     * @param $validator
     */
    public function parseValid($attribute, $params, $validator)
    {
        if(!$this->initParse())
            $this->addError($attribute,'Не валидный документ');

    }


    /**
     * Инициация парсинга документа с проверкой его на вализность
     * @return bool
     */
    private function initParse()
    {
        $this->dom = SimpleHTMLDom::file_get_html($this->fileHtml->tempName);
        if($this->dom){
            $this->nodes = $this->dom->find('tr');
            return count($this->nodes) > 0;
        }

        return false;
    }


    /**
     * Формирование данных для графика
     * @return array
     */
    public function dataCharts()
    {
        $data = [];
        $profit = 0;
        foreach ($this->transactions as $id=>$transaction){
            $profit += $transaction->profit;
            $data[$id] = $profit;
        }

        return  [
            'labels' => array_keys($data),
            'datasets' => [
                [
                    'data' => array_values($data),
                    'label' =>  "График депозита",
                    'fill' => false,
                    'lineTension' => 0.1,
                    'backgroundColor' => "rgba(75,192,192,0.4)",
                    'borderColor' => "rgba(75,192,192,1)",
                    'borderCapStyle' => 'butt',
                    'borderDash' => [],
                    'borderDashOffset' => 0.0,
                    'borderJoinStyle' => 'miter',
                    'pointBorderColor' => "rgba(75,192,192,1)",
                    'pointBackgroundColor' => "#fff",
                    'pointBorderWidth' => 1,
                    'pointHoverRadius' => 5,
                    'pointHoverBackgroundColor' => "rgba(75,192,192,1)",
                    'pointHoverBorderColor' => "rgba(220,220,220,1)",
                    'pointHoverBorderWidth' => 2,
                    'pointRadius' => 1,
                    'pointHitRadius' => 10,
                    'spanGaps' => false,
                ]
            ]
        ];
    }

    /**
     * Парсинг документа и формирование массива транзакций валидного документа
     * @return bool
     */
    public function parsing()
    {
        if ($this->validate()){
            $c = [];
            foreach ($this->nodes as $node){
                /** @var simple_html_dom_node $tr */

                if($node->nodeName() == 'tr'){
                    $transaction =  new Transaction($node);
                    if($transaction->validate())
                        $this->transactions[$transaction->id] = $transaction;
                }
            }
            return count($this->transactions);
        }
        return false;
    }

    /**
     * Начальное значение депозита
     * @return float|int
     */
    public function beginBalance()
    {
        foreach ($this->transactions as $transaction)
            return $transaction->profit;
        return 0;
    }

    /**
     * Последнее значение депозита
     * @return float|int
     */
    public function endBalance()
    {
        $profit = 0;
        foreach ($this->transactions as $transaction)
            $profit += $transaction->profit;
        return $profit;
    }

}