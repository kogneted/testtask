<?php

namespace app\controllers;

use app\models\Parser;
use keltstr\simplehtmldom\simple_html_dom_node;
use keltstr\simplehtmldom\SimpleHTMLDom;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }




    public function actionIndex()
    {
        $model = new Parser();

        if(Yii::$app->request->isPost){
            $model->fileHtml = UploadedFile::getInstance($model,'fileHtml');
            if($model->parsing())
                return $this->render('charts',[
                    'model'=>$model
                ]);

        }

        return $this->render('index',[
            'model'=>$model
        ]);
    }


}
