<?php

/* @var $this yii\web\View */
/* @var $model Parser */

use app\models\Parser;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']])?>

        <?= $form->field($model,'fileHtml')->fileInput()->label("Выбрать отчет")?>

        <?= \yii\helpers\Html::submitButton('Распарсить отчет',['class'=>'btn btn-success'])?>

        <?php ActiveForm::end()?>
    </div>

</div>
