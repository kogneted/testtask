<?php
/**
 * @var $model Parser
 */
use app\models\Parser;


?>


<div>

    <p>
        <?=\yii\helpers\Html::a('< Выбрать другой файл',['index'],['class'=>'btn btn-info'])?>
    </p>
    <hr>
    <div class="row">
        <div class="col-lg-6 text-center">
            <h3>Начальный баланс</h3>
            <b><?= $model->beginBalance()?></b>
        </div>
        <div class="col-lg-6 text-center">
            <h3>Конечный баланс</h3>
            <b><?= $model->endBalance()?></b>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?=\phpnt\chartJS\ChartJs::widget([
                'type'  => \phpnt\chartJS\ChartJs::TYPE_BAR,
                'data'  => $model->dataCharts(),
                'options'   => []
            ])?>
        </div>

    </div>

</div>
